/* eslint-disable indent */
export const state = () => ({
  userSettings: [],
  csrSettings: []
});

export const mutations = {
  SET_USER_SETTING(state, userSettings) {
    state.userSettings = userSettings;
  },
  SET_CSR_SETTING (state, csrSettings) {
    state.csrSettings = csrSettings;
  },
  SET_CSR_CONTENT_SETTING (state, contentSettings) {
    state.contentSettings = contentSettings;
  }
};

export const actions = {
  async getUserSettings({ commit }) {
    const response = await this.$axios.post("AUTH_URL/v1/getUserSetting");
    const info = response.data.message;
    commit("SET_USER_SETTING", info);
  },
  async getCsrSettings ({commit}) {
    const response = await this.$axios.get("AUTH_URL/v1/getCsrSetting");
    const data = response.data.settings;
    const content = response.data.contentSettings;
    commit("SET_CSR_SETTING", data)
    commit("SET_CSR_CONTENT_SETTING", content)
  }
};

