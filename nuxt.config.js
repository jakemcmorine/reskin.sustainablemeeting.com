function configuration(environement) {
  if (environement === "local") {
    return {
      AUTH_URL: process.env.AUTH_URL || "http://localhost:4000",
      USER_URL: process.env.USER_URL || "http://localhost:3003",
      CUR_URL: process.env.CUR_URL || "http://localhost:3000",
      S3_PREFIX:
        process.env.S3_PREFIX ||
        "https://sustainable-meeting-dev.s3-eu-west-1.amazonaws.com"
    };
  } else if (environement === "development") {
    return {
      AUTH_URL:
        process.env.AUTH_URL || "https://dev-auth-ss.emvigotechnologies.com",
      USER_URL:
        process.env.USER_URL || "https://dev-user.ss.emvigotechnologies.com",
      CUR_URL:
        process.env.CUR_URL || "https://dev-reskin.ss.emvigotechnologies.com",
      S3_PREFIX:
        process.env.S3_PREFIX ||
        "https://sustainable-meeting-dev.s3-eu-west-1.amazonaws.com"
    };
  } else if (environement === "production") {
    return {
      AUTH_URL:
        process.env.AUTH_URL || "https://auth2-meetings.sustainably.run",
      USER_URL: process.env.USER_URL || "https://app2-meetings.sustainably.run",
      CUR_URL: process.env.CUR_URL || "https://meetings.sustainably.run",
      S3_PREFIX:
        process.env.S3_PREFIX ||
        "https://sustainable-meeting-prod.s3-eu-west-1.amazonaws.com"
    };
  } else {
    console.error(
      "Define environment before running like NODE_ENV=local npm run build"
    );
    throw new Error(
      "Undefined environment, Define environment before running like NODE_ENV=local npm run build"
    );
  }
}

export default {
  mode: "spa",
  /*
   ** Headers of the page
   */

  collapseBooleanAttributes: true,
  decodeEntities: true,
  minifyCSS: false,
  minifyJS: true,
  processConditionalComments: true,
  removeEmptyAttributes: true,
  removeRedundantAttributes: true,
  trimCustomFragments: true,
  useShortDoctype: true,

  head: {
    // title: process.env.npm_package_name || "",
    title: "Sustainably Run Meetings",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],

    script: [
      {
        src:
          configuration(process.env.NODE_ENV).USER_URL +
          "/js/jquery-3.3.1.min.js"
      },
      //{ src: "/assets/js/popper.min.js" },
      //{ src: "/assets/js/bootstrap.min.js" },
      // { src: "/assets/js/custom.js" },

      {
        src:
          "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      },
      { src: "https://code.jquery.com/jquery-3.3.1.slim.min.js" },
      {
        src:
          "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
      },
      { src: "//js.hs-scripts.com/6032362.js", async: true, defer: true },
      
    ],
    link: [
      { rel: "icon", type: "image/png", href: "/SR-favicon.png" },
      {
        rel: "stylesheet",
        href:
          "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
      },
      {
        rel: "stylesheet",
        type: "text/css",
        href:
          "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
      }
      // {
      //   rel: "stylesheet",
      //   type: "text/css",
      //   href: "https://allfont.net/allfont.css?fonts=roboto-bold"
      // },
      // {
      //   rel: "stylesheet",
      //   href: "https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap"
      // }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },
  /*
   ** Global CSS
   */
  css: [
    { src: "assets/css/style.css", lang: "css" },
    // { src: "assets/css/iEdit.css", lang: "css" },
    { src: "assets/css/bootstrap.min.css", lang: "css" },
    { src: "assets/css/bootstrap.css", lang: "css" },
    "quill/dist/quill.snow.css",
    "quill/dist/quill.bubble.css",
    "quill/dist/quill.core.css"
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: "@/plugins/chart", ssr: false },
    { src: "@/plugins/editor.js", ssr: false },
    { src: "~/plugins/vue-datepicker", ssr: false } // datepicker plugin here

    // 'vue-chartjs'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    "@nuxtjs/vuetify",
    // Doc: https://github.com/nuxt-community/eslint-module
    "@nuxtjs/eslint-module",
    "@nuxtjs/moment",
    // "@nuxtjs/gtm",
    "@nuxtjs/dotenv"
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    "@nuxtjs/auth",
    "@nuxtjs/gtm",
    "@tui-nuxt/editor",
    '@nuxtjs/recaptcha',
    ['nuxt-facebook-pixel-module', {
      /* module options */
      track: 'PageView',
      pixelId: '159144412687065',
      autoPageView: true,
      disabled: false
    }],
  ],
  recaptcha: {
    hideBadge: true,
    siteKey: "6Ledx4kaAAAAAJFPFH6yXL8vt9MZ7xiu64MQy_0z",
    version: 3
  },
  gtm: {
    
    enabled: true, /* see below */
    debug: false,

    id: "GTM-5F3FLPQ",
    layer: 'dataLayer',
    variables: {},

    // pageTracking: false,
    // pageViewEventName: 'Sustainably Run Meetings',

    autoInit: true,
    respectDoNotTrack: true,

    scriptId: 'gtm-script',
    scriptDefer: false,
    scriptURL: 'https://www.googletagmanager.com/gtm.js',
    crossOrigin: false,

    noscript: true,
    noscriptId: 'gtm-noscript',
    noscriptURL: 'https://www.googletagmanager.com/ns.html'
  },
  tui: {
    editor: true // or options
  },

  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    AUTH_URL: configuration(process.env.NODE_ENV).AUTH_URL,
    USER_URL: configuration(process.env.NODE_ENV).AUTH_URL,
    CUR_URL: configuration(process.env.NODE_ENV).AUTH_URL,
    proxy: true
  },

  proxy: {
    "/AUTH_URL/": {
      target: configuration(process.env.NODE_ENV).AUTH_URL,
      pathRewrite: { "^/AUTH_URL/": "" },
      changeOrigin: true
    },
    "/USER_URL/": {
      target: configuration(process.env.NODE_ENV).USER_URL,
      pathRewrite: { "^/USER_URL/": "" },
      changeOrigin: true
    },
    "/S3_PREFIX/": {
      target: configuration(process.env.NODE_ENV).S3_PREFIX,
      pathRewrite: { "^/S3_PREFIX/": "" },
      changeOrigin: true
    },
    "/certificate/": {
      target: configuration(process.env.NODE_ENV).USER_URL + "/certificate2/",
      pathRewrite: { "^/certificate/": "" },
      changeOrigin: true
    },
    "/showcase/": {
      target: configuration(process.env.NODE_ENV).USER_URL + "/csrHub/",
      pathRewrite: { "^/showcase/": "" },
      changeOrigin: true
    },
    "/demoCertificate/": {
      target:
        configuration(process.env.NODE_ENV).USER_URL + "/demo/certificate/",
      pathRewrite: { "^/demoCertificate/": "" },
      changeOrigin: true
    },
    "/download/plugin": {
      target: configuration(process.env.NODE_ENV).USER_URL + "/download/plugin",
      pathRewrite: { "^/download/plugin": "" },
      changeOrigin: true
    },
    "/download/outlook-web-plugin": {
      target:
        configuration(process.env.NODE_ENV).USER_URL +
        "/download/outlook-web-plugin",
      pathRewrite: { "^/download/outlook-web-plugin": "" },
      changeOrigin: true
    },
    "/.well-known/microsoft-identity-association.json": {
      target:
        configuration(process.env.NODE_ENV).USER_URL +
        "/.well-known/microsoft-identity-association.json",
      pathRewrite: { "^/.well-known/microsoft-identity-association.json": "" },
      changeOrigin: true,
      proxyHeadersIgnore: ["Transfer-Encoding"]
    }
  },

  /*
   ** Build configuration
   */
  build: {
    /*
  
          ** You can extend webpack config here
          */
    extractCSS: true,

    extend(config, ctx) {}
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "AUTH_URL/v1/login",
            method: "post",
            propertyName: "data.token"
          },
          logout: { url: "AUTH_URL/v1/logout", method: "post" },
          // logout: false,

          user: {
            url: "AUTH_URL/v1/checkAuth",
            method: "get",
            propertyName: "data"
          }
        }
        // tokenRequired: true,
        // tokenType: '',
        // globalToken: true,
        // autoFetchUser: true
      }
    }
  },

  cookie: true,
  localStorage: true,
  token: {
    prefix: "_data."
  }
};
