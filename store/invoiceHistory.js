/* eslint-disable indent */
export const state = () => ({
  invoice: [],
  userDetails: [],
  subscribe: [],
  invoiceStatus: []
});

export const mutations = {
  SET_INVOICE(state, invoice) {
    state.invoice = invoice;
  },
  SET_USERDETAILS(state, userDetails) {
    state.userDetails = userDetails;
  },
  SET_SUBSCRIBE(state, subscribe) {
    state.subscribe = subscribe;
  },
  SET_INVOCE_STATUS(state, invoiceStatus) {
    state.invoiceStatus = invoiceStatus;
  }
};

export const actions = {
  async loadUserDetails({ commit }) {
    const response = await this.$axios.get("USER_URL/user-details-reskin");
    commit("SET_USERDETAILS", response.data);
  },
  async loadInvoice({ commit }) {
    const info = await this.$axios.get("USER_URL/invoiceList");
    commit("SET_INVOICE", info.data);
  },
  async getSubscribeDetails({ commit }) {
    const info = await this.$axios.get("USER_URL/getIPStatus-reskin");
    commit("SET_SUBSCRIBE", info.data);
  },
  async getInvoiceStatus({ commit }) {
    const info = await this.$axios.get("USER_URL/subscribeStatus");
    // console.log("PENDING DETAILS :", info)
    commit("SET_INVOCE_STATUS", info.data);
  }
};
