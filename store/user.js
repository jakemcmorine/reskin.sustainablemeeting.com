/* eslint-disable indent */
export const state = () => ({
  user: [],
  count: [],
  treecount: [],
  domainItems: [],
  ambassador_trees: []
});

export const mutations = {
  SET_USER(state, user) {
    state.user = user;
  },
  SET_UPDATE_USER(state, user) {
    state.user = [];
    state.user = user;
  },
  SET_MEETINGS_COUNT(state, count) {
    state.count = count;
  },
  SET_TREE_COUNT(state, treecount) {
    state.treecount = treecount;
  },
  SET_DOMAINS(state, domainItems) {
    state.domainItems = domainItems;
  },
  SET_AMBASSADOR_TREE(state, count) {
    state.ambassador_trees = count;
  }
};

export const actions = {
  async loadUser({ commit }) {
    const response = await this.$axios.post("AUTH_URL/v1/getAccountDet");
    const info = response.data.message;
    commit("SET_USER", info);
  },
  async updateAccountDetail({ commit }, user) {
    commit("SET_UPDATE_USER", user);
    await this.$axios.post("AUTH_URL/v1/saveDetail", user);
    return user;
  },
  async getCount({ commit }) {
    const response = await this.$axios.post("USER_URL/meetings/count-reskin");
    // console.log("response for count", response);
    var no_of_meetings = 0,
      no_of_trees = 0;
    if (
      response === undefined ||
      response.data === undefined ||
      response.data.meetingCount === undefined
    ) {
      no_of_meetings = 0;
    } else {
      no_of_meetings = response.data.meetingCount;
    }
    var meetingArray = ("" + no_of_meetings).split("");

    let len;
    if(meetingArray.length > 4){
      len = 6 - meetingArray.length;
    } else {
      len = 4 - meetingArray.length;
    }
    if (len > 0) {
      for (var i = 0; i < len; i++) {
        meetingArray.unshift("0");
      }
    }
    commit("SET_MEETINGS_COUNT", meetingArray);

    if (
      response === undefined ||
      response.data === undefined ||
      response.data.treeCount === undefined ||
      response.data.treeCount[0].treeCount === undefined ||
      response.data.treeCount[0].treeCount === null
    ) {
      no_of_trees = 0;
    } else {
      no_of_trees = parseInt(response.data.treeCount[0].treeCount);
    }
    // console.log("no_of_trees", no_of_trees);
    if (response && response.data) {
      // console.log("initital tree count", response.data.initialCount.message.treeNumber);
      no_of_trees += parseInt(response.data.initialCount.message.treeNumber);
    }
    var treeArray = ("" + no_of_trees).split("");

    if(treeArray.length > 4){
      len = 6 - treeArray.length;
    } else {
      len = 4 - treeArray.length;
    }
    if (len > 0) {
      for (var i = 0; i < len; i++) {
        treeArray.unshift("0");
      }
    }
    commit("SET_TREE_COUNT", treeArray);
  },
  async getAmbassadorCount({ commit }) {
    var no_of_trees;
    const response = await this.$axios.get("AUTH_URL/v1/getAmbassadorCount");

    if (
      response === undefined ||
      response.data === undefined ||
      response.data == null ||
      response.data.trees === undefined ||
      response.data.trees == null
    ) {
      no_of_trees = 0;
    } else {
      no_of_trees = response.data.trees;
    }
    var treeArray = ("" + no_of_trees).split("");
    let len;
    if(treeArray.length > 4){
      len = 6 - treeArray.length;
    } else {
      len = 4 - treeArray.length;
    }
    if (len > 0) {
      for (var i = 0; i < len; i++) {
        treeArray.unshift("0");
      }
    }
    commit("SET_AMBASSADOR_TREE", treeArray);
  },
  async getAllDomains({ commit }) {
    const response = await this.$axios.post("AUTH_URL/v1/getAllDomainByUser");
    const info = response.data.resultData;
    commit("SET_DOMAINS", info);
  }
};
