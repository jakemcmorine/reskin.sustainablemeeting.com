import axios from "axios";

export const state = () => ({
  count: null
});
export const mutations = {
  SET_COUNT(state, count) {
    state.count = count;
  }
};
export const actions = {
  async getCount({ commit }) {
    const response = await this.$axios.post(
      "USER_URL/meetings/count-reskin",
      {}
    );
    //const info = response.data.message;
    // console.log("count details in store :", data.meetingCount);
    // commit("SET_COUNT", info);
  }
};

/* export const mutations = {
  init(state) {
    axios.get("/user/meetings/count").then(res => {
      state.count = res.data;
    });
  },

  list(state, count) {
    state.count.push(count);
  }
}; */
