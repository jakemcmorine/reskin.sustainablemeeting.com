let required = (propertyType, customErrorMessage) => {
  return v =>
    (v && v.length > 0) || customErrorMessage || `Enter ${propertyType}`;
};
let minLength = (propertyType, minLength) => {
  return v => {
    if (!v) {
      return true;
    }
    return (
      v.length >= minLength ||
      `${propertyType} must be at least ${minLength} characters`
    );
  };
};

let maxLength = (propertyType, maxLength) => {
  return v =>
    (v && v.length <= maxLength) ||
    `${propertyType} must be less than ${maxLength} characters`;
};

let emailFormat = () => {
  let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/;

  // let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,50})+$/;
  return v => (v && regex.test(v)) || "Enter valid email";
};

let emailFormatinvite = () => {
  let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,24})+$/;
  return v => (v && regex.test(v)) || "Enter valid email";
};

let pwdConfirm = value => {
  return v => v === value || "Passwords must match";
};

let emailChecking = (list, currentEmail) => {
  // console.log("in Validation ....... :", list, currentEmail)
  if (list.length > 0) {
    const foundItem = list.find(item => {
      return item.email === currentEmail;
    });
    if (foundItem === undefined) {
      return true;
    } else {
      return "This invitee is already exist in the list.";
    }
  } else {
    return true;
  }
};

let checkEmail = value => {
  return v => v === value || "Password not changed";
};

let urlFormat = () => {
  let regex = /^((ftp|http|https):\/\/)?(www.)?(?!.*(ftp|http|https|www.))[a-zA-Z0-9_-]+(\.[a-zA-Z]+)+((\/)[\w#]+)*(\/\w+\?[a-zA-Z0-9_]+=\w+(&[a-zA-Z0-9_]+=\w+)*)?$/;
  // let regex = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=-]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=-]*)/g;
  return v => (v && regex.test(v)) || "Enter valid url";
};

let urlFormatSignup = () => {
  let regex = /^((ftp|http|https):\/\/)?(www.)?(?!.*(ftp|http|https|www.))[a-zA-Z0-9_-]+(\.[a-zA-Z\-\_]+)+((\/)[\w\-#]+)*(\/\w+\?[a-zA-Z0-9_]+=\w+(&[a-zA-Z0-9_]+=\w+)*)?\/?$/;
  // let regex = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=-]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=-]*)/g;
  return v => (v && regex.test(v)) || "Enter valid url";
};

let youtubeUrlValid = () => {
  let regex = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  return v => (v && regex.test(v)) || "Enter valid url";
};

let phoneMinLength = (propertyType, phoneMinLength) => {
  return v => {
    if (!v) {
      return true;
    }

    return (
      v.length >= phoneMinLength ||
      `${propertyType} must be at least ${phoneMinLength} digit`
    );
  };
};

let phoneMaxLength = (propertyType, phoneMaxLength) => {
  return v =>
    (v && v.length <= phoneMaxLength) ||
    `${propertyType} must be less than ${phoneMaxLength} digit`;
};

let pwdMinLength = pwdMinLength => {
  return v => {
    if (!v) {
      return true;
    }

    return (
      v.length >= pwdMinLength ||
      `Password must contain minimum ${pwdMinLength} characters`
    );
  };
};

let pwdMaxLength = pwdMaxLength => {
  return v =>
    (v && v.length <= pwdMaxLength) ||
    `Password must not exceed  ${pwdMaxLength} characters`;
};

export default {
  required,
  minLength,
  maxLength,
  emailFormat,
  emailFormatinvite,
  pwdConfirm,
  checkEmail,
  urlFormat,
  urlFormatSignup,
  youtubeUrlValid,
  phoneMinLength,
  phoneMaxLength,
  pwdMinLength,
  pwdMaxLength,
  emailChecking
};
