export default function({ $auth, redirect, store, route }) {
  if (route.name != "csr-hub-all") {
    if (!$auth.loggedIn) {
      store.dispatch("snackbar/setSnackbar", {
        text: "You must sign in to access this page",
        timeout: 2000
      });
      return redirect(`/?redirect=${route.path}`);
    } else {
      var user = JSON.parse(localStorage.getItem("UserDetails"));
      let user_routes = [
        "/userPassword",
        "/userDashboard",
        "/userFeedback",
        "/userBugs"
      ];
      let demo_ip_routes = ["/meeting", "/setup", "/csrhub"];
      let demo_meeting = ["/meeting"];
      let userType = user.type;
      if (userType == "initiative_partner") {
        // let ip_routes = ['/setup', '/Dashboard', '/Inviteusers', '/account', '/invoice', '/dnsVerification', '/feedback','/bugs'];
        if (
          user_routes.includes(route.path) ||
          demo_meeting.includes(route.path)
        ) {
          return redirect("/setup");
        }
      } else if (userType == "employee") {
        if (!user_routes.includes(route.path)) {
          return redirect("/userDashboard");
        }
      } else if (userType == "demo_ip") {
        // console.log("I am a demo ip");
        if (!demo_ip_routes.includes(route.path)) {
          return redirect("/setup");
        }
      } else {
        console.log("Now I am a hacker");
      }
    }
  }
}
