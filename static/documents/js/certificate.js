$(function () {
    var fromDiv = $('#hiddenDiv').attr('data-clientid');
    console.log("clientid", fromDiv, "end");
    var d = new Date();
    var n = d.getFullYear();
    $("#currYearFoot").html(n);

    $(".cerBxSocial").on("click", function (e) {
        $(".certificatepagelinks1").addClass("open");
        e.stopPropagation();
        // Get Hubspot client id from the CSR HUB page.
        var hubspotId = $('#hiddenDiv').attr('data-clientid');
        console.log("on click ", hubspotId);

        // Social Media Sharing
        $("#shareRoundIcons").jsSocials({
            url: "_url_",
            showLabel: false,
            showCount: false,
            _createShareLink: function (share) {
                var $result = jsSocials.Socials.prototype._createShareLink.apply(this, arguments);
                var pageUrl = decodeURIComponent(window.location.href);
                var queryString = window.location.search;
                console.log("queryString", queryString);
                const urlParams = new URLSearchParams(queryString);

                if (urlParams.has('utm_source')) {
                    pageUrl = pageUrl.replace(urlParams.get('utm_source'), share.share);
                } else {
                    if (queryString) {
                        pageUrl = pageUrl.concat('&utm_source=' + share.share);
                    } else {
                        pageUrl = pageUrl.concat('?utm_source=' + share.share);
                    }
                }

                if (urlParams.has('utm_campaign')) {
                    pageUrl = pageUrl.replace(urlParams.get('utm_campaign'), "emailed_certificate");
                } else {
                    pageUrl = pageUrl.concat('&utm_campaign=emailed_certificate');
                }

                if (urlParams.has('utm_term')) {
                    pageUrl = pageUrl.replace(urlParams.get('utm_term'), hubspotId);
                } else {
                    pageUrl = pageUrl.concat('&utm_term=' + hubspotId);
                }
                $result.on("click", function () {
                    // replace the your placeholder with actual url
                    const replaceUrl = encodeURIComponent(pageUrl);
                    var href = $result.attr("href");
                    $result.attr("href", href.replace("_url_", replaceUrl));
                    console.log("After replacing the placeholder", $result.attr("href"))
                });
                return $result;
            },
            shares: ["twitter", "facebook", "linkedin"]
        });
    });
    $(document).on("click", function (e) {
        if ($(e.target).is(".cerBxSocial") === false) {
            $(".certificatepagelinks1").removeClass("open");
        }
    });
});