/* eslint-disable indent */
export const state = () => ({
  inviteduser: [],
  ipTreeNumber: 0,
});

export const mutations = {
  SET_ALL_USER(state, inviteduser) {
    state.inviteduser = inviteduser;
  },
  SET_IP_TREE(state, treeCount) {
    state.ipTreeNumber = treeCount;
  }
  /* SET_INVITED_USERS(state, manual_inviteduser) {
    state.inviteduser = state.inviteduser.concat(manual_inviteduser);
    console.log("SET_INVITED_USERS: ", state.inviteduser);
  } */
};

export const actions = {
  async getAllusers({ commit }) {
    const response = await this.$axios.get("AUTH_URL/v1/getAllUser");
    commit("SET_ALL_USER", response.data.data);
  },
  async getIPTreeConfig({commit}) {
    const response = await this.$axios.get("AUTH_URL/v1/getIPTreeConfig");
    commit("SET_IP_TREE", response.data);
  }
};
