Vue.use(VeeValidate);
const app = new Vue({
  el: "#brochure",
  data: {
    downloadbrochure: {},
    isMedia: this.isMedia
  },
  methods: {
    downloadBrchr(e) {
      e.preventDefault();
      this.$validator.validate().then(valid => {
        if (valid) {
          // console.log("download brocher data", this.downloadbrochure, user_url);

          // console.log("Update Email click :");
          var link = document.createElement("a");
          link.download = "SRM-brochure.pdf";
          link.href = "/documents/SRM-brochure.pdf";
          link.click();
          $("#exampleModalCenter").modal("hide");
          axios
            .post(user_url + "/saveToHubspot", this.downloadbrochure)
            .then(response => {
              // console.log("response", response);
            })
            .catch(error => {
              console.log("error", error);
            });
        } else {
        }
      });
    }
  }
});

$(function() {
  // console.log("isSample", isSample);
  // console.log("isDemo", isDemo);
  // console.log("user_url", user_url);
  var d = new Date();
  var n = d.getFullYear();
  $("#currYearFoot").html(n);

  $("#exampleModalCenter").on("hidden.bs.modal", function() {
    $("#exampleModalCenter form")[0].reset();
  });

  $(".cerBxSocial").on("click", function(e) {
    $(".certificatepagelinks1").toggleClass("open");
    e.stopPropagation();

    // console.log("on click ", hubspotId);

    // Social Media Sharing
    if (isDemo === true) {
      // console.log("SAMPLE CERTIFICATE");
      $("#shareRoundIcons").jsSocials({
        url: "https://www.sustainably.run/meeting",
        showLabel: false,
        showCount: false,
        shares: ["twitter", "facebook", "linkedin"]
      });
    } else {
      $("#shareRoundIcons").jsSocials({
        url: "_url_",
        showLabel: false,
        showCount: false,
        _createShareLink: function(share) {
          var $result = jsSocials.Socials.prototype._createShareLink.apply(
            this,
            arguments
          );
          var pageUrl = decodeURIComponent(window.location.href);
          var queryString = window.location.search;
          // console.log("queryString", queryString);
          const urlParams = new URLSearchParams(queryString);

          if (urlParams.has("utm_source")) {
            pageUrl = pageUrl.replace(urlParams.get("utm_source"), share.share);
          } else {
            if (queryString) {
              pageUrl = pageUrl.concat("&utm_source=" + share.share);
            } else {
              pageUrl = pageUrl.concat("?utm_source=" + share.share);
            }
          }

          if (urlParams.has("utm_campaign")) {
            pageUrl = pageUrl.replace(
              urlParams.get("utm_campaign"),
              "emailed_certificate"
            );
          } else {
            pageUrl = pageUrl.concat("&utm_campaign=emailed_certificate");
          }

          if (urlParams.has("utm_term")) {
            pageUrl = pageUrl.replace(urlParams.get("utm_term"), hubspotId);
          } else {
            pageUrl = pageUrl.concat("&utm_term=" + hubspotId);
          }
          $result.on("click", function() {
            // replace the your placeholder with actual url
            const replaceUrl = encodeURIComponent(pageUrl);
            var href = $result.attr("href");
            $result.attr("href", href.replace("_url_", replaceUrl));
            // console.log(
            //   "After replacing the placeholder",
            //   $result.attr("href")
            // );
          });
          return $result;
        },
        shares: ["twitter", "facebook", "linkedin"]
      });
    }
  });
  $(document).on("click", function(e) {
    if ($(e.target).is(".cerBxSocial") === false) {
      $(".certificatepagelinks1").removeClass("open");
    }
  });
});
